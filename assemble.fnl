#!/bin/env fennel

(fn fnlBuild [input]
  "use the fennel program to \"build\" a file and write the lua to another file
expected args are {:src source-file :tgt target-file}"
  (os.execute (table.concat ["fennel" "--compile" input.src ">>" input.tgt] " "))

  )
(fn install [src dest args]
  (os.execute
   (table.concat ["install " "-C" ""  src dest] " ")
   )
  )
(os.execute "echo '#!/bin/env luajit\n'>cfgMenu.lua")

(fnlBuild {:src "cfgMenu.fnl" :tgt "cfgMenu.lua"})

(install "cfgMenu.lua" "/home/erik/bin/cfgMenu")
