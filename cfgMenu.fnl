#!/bin/env fennel
;; -*- Mode : outline-minor; Mode: fennel; -*-
(local fennel (require					:fennel))
(local configPath
	   (..
		(or (os.getenv "XDG_CONFIG_DIR")
			(.. (os.getenv "HOME") "/.config"))
		"/fennelScripts"))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(set package.path (.. package.path configPath))
(set fennel.path (.. fennel.path ";" configPath "/?.fnl"))

(if (not= (os.getenv "TERM") nil)
	(os.execute "sleep 2")
	)

(fn dmenuBuild [sep kvpairs opts]
	(local key opts.key)
	(local value opts.val)
	(print opts.key opts.val)
	(each [k v (ipairs kvpairs)]
		(print (.. (. v opts.key) sep (. v opts.val)))
	)
)


(local rc (require						:cfgMenurc))
(local settings (require				:general))
(local dmenu (include					:dmenu))
(local configFiles rc.configFiles)
(local cfgMenu {})



(fn geninput [sep kvpairs opts]
  (local key opts.key)
  (local value opts.val)
  (print opts.key opts.val)
  (local outvar [])
  (each [k v (ipairs kvpairs)]
	(table.insert outvar (.. (. v opts.key) sep (. v opts.val)))
	)
  outvar
  )

(local pathToOpen
	   (. (dmenu.capture
		   (dmenu.show
			(geninput ":" configFiles {:key "alias" :val "path"})
			(dmenu.presets.centered
			 {
			  :prompt "test:"
			  :separator ":"
			  :columns 1
			  }
			 )
			)
		   )
		  :out))

;;(dmenuBuild ":" vals {				:key "k" :val "v"})
;;(dmenuBuild ":" configPaths {			:key "alias" :val "path"})
(fn final [pth]
  (if (not= pth "" " " nil)
	  (os.execute (settings.editorFn pth))
	  )
  )
(final pathToOpen)
